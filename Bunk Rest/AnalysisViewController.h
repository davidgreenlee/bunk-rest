//
//  AnalysisViewController.h
//  Crew Rest
//
//  Created by David Greenlee on 13/09/2012.
//  Copyright (c) 2012 David Greenlee. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AnalysisViewController : UITableViewController <UITableViewDelegate, UITableViewDataSource>

extern NSString *kAlarmDataKey;

@property (nonatomic, strong) NSArray *interfaceDetailData;
@property (nonatomic, strong) NSString *reminderText;

- (void)setupInterface;
- (void)showTimerView;
- (void)scheduleNotification;

@end
