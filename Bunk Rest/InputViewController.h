//
//  InputViewController.h
//  Crew Rest
//
//  Created by David Greenlee on 13/09/2012.
//  Copyright (c) 2012 David Greenlee. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface InputViewController : UITableViewController <UIActionSheetDelegate, UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) NSArray *interfaceData;
@property (nonatomic, strong) NSArray *interfaceDetailData;

@end
