//
//  InputViewController.m
//  Crew Rest
//
//  Created by David Greenlee on 13/09/2012.
//  Copyright (c) 2012 David Greenlee. All rights reserved.
//

#import "InputViewController.h"
#import "RestModel.h"
#import "DateCategory.h"
#import "TimeViewController.h"
#import "AnalysisViewController.h"
#import "AboutViewController.h"

@interface InputViewController ()

@end

@implementation InputViewController

@synthesize interfaceData, interfaceDetailData;

- (id)init
{
    self = [super initWithStyle:UITableViewStyleGrouped];
    if (self) {
        // Custom initialization
        [self setTitle:@"Bunk Rest"];
        
        NSArray *section0 = [[NSArray alloc] initWithObjects:@"Help", nil];
        
        NSArray *section1 = [[NSArray alloc]initWithObjects:@"Breaks Start", @"ETA", nil];
        
        NSArray *section2 = [[NSArray alloc] initWithObjects:@"Calculate the rest breaks", nil];
        
        NSArray *section3 = [[NSArray alloc]initWithObjects:@"Time between End & ETA", @"Handover duration", @"Number of breaks", nil];
        
        
        
        
        
        interfaceData = [[NSArray alloc] initWithObjects:section0, section1, section2, section3, nil];
    }
    return self;
}

-(BOOL)canBecomeFirstResponder {
    return YES;
}


- (id)initWithStyle:(UITableViewStyle)style
{
    return [self init];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidAppear:(BOOL)animated {
    // NSLog(@"viewDidAppear");
    
	[super viewDidAppear:animated];
    [self becomeFirstResponder];
	// [self setupInterface];
	[self.tableView reloadData];
}

- (void)viewWillDisappear:(BOOL)animated {
    [self resignFirstResponder];
    [super viewWillDisappear:animated];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)motionEnded:(UIEventSubtype)motion withEvent:(UIEvent *)event
{
    if (motion == UIEventSubtypeMotionShake)
    {
        // your code
        // NSLog(@"reset timings");
        
        [[UIApplication sharedApplication] cancelAllLocalNotifications]; // remove any alarms
        
        RestModel *model = [RestModel sharedRestModel];
        
        [NSTimeZone setDefaultTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
        
        NSDate *date = [[NSDate alloc] init];
        NSTimeInterval secs = [date timeIntervalSinceReferenceDate];
        
        secs = secs - fmod(secs, 300.0); // now set to integer 5 minutes to match UIPicker
        
        model.appBecameActive = secs; // now
        model.breaksStart = secs;
        model.eTA = secs + 7*3600; // now + 7 hours
        model.alarm0IsOn = NO;
        model.alarm1IsOn = NO;
        model.alarm2IsOn = NO;
        
        [self.tableView reloadData];
    }
}

- (void)alarmSwitched:(id)sender
{
    RestModel *model = [RestModel sharedRestModel];
    
    if (model.alarm0IsOn) {
        model.alarm0IsOn = NO;
        [[UIApplication sharedApplication] cancelAllLocalNotifications];
    } else {
        model.alarm0IsOn = YES;
    }
}

#pragma mark Table view methods

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
	switch (section) {
		case 0: return @"Bunk Rest";
			break;
        case 1: return @"This Sector";
			break;
		case 3: return @"Setup options";
			break;
		default: return nil;
	}
}

- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section {
    switch (section) {
		case 0: return nil;
			break;
        case 1: return @"Times are GMT";
			break;
		case 3: return @"Tap to cycle through possible values";
			break;
		default: return nil;
	}

}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [interfaceData count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[interfaceData objectAtIndex:section] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    RestModel *model = [RestModel sharedRestModel];
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
    }
    
    [[cell textLabel] setText:[[interfaceData objectAtIndex:indexPath.section] objectAtIndex:indexPath.row]];
    
    if (indexPath.section == 0) {
        cell.accessoryView = nil;
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.detailTextLabel.text = nil;
        cell.backgroundColor = [UIColor whiteColor];
        return cell;
    }
    
    if (indexPath.section == 1) {
        if (indexPath.row == 0) { // Breaks start
            [NSTimeZone setDefaultTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
            NSDate *start = [NSDate dateWithTimeIntervalSinceReferenceDate:model.breaksStart];
            cell.detailTextLabel.text = [start detailDescription];
        } else if (indexPath.row == 1) { // ETA
            [NSTimeZone setDefaultTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
            NSDate *landing = [NSDate dateWithTimeIntervalSinceReferenceDate:model.eTA];
            cell.detailTextLabel.text = [landing detailDescription];
        }
    }
    
    if (indexPath.section == 2) {
        cell.accessoryView = nil;
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.detailTextLabel.text = nil;
        cell.backgroundColor = [UIColor colorWithRed:0.25f green:0.5f blue:1.0f alpha:1.0f];
        return cell;
    }
    
    if (indexPath.section == 3) {
        if (indexPath.row == 0) { // finalService
            cell.detailTextLabel.text = [model descriptionForSecs:model.finalService];
            return cell;
        }
        if (indexPath.row == 1) { // handover
            cell.detailTextLabel.text = [model descriptionForSecs:model.handover];
            return cell;
        }
        if (indexPath.row == 2) { // number of breaks
            cell.detailTextLabel.text = [NSString stringWithFormat:@"%d", model.numberOfBreaks];
            return cell;
        }
    }

    return  cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    RestModel *model = [RestModel sharedRestModel];
    
    if (indexPath.section == 0) {
		if (indexPath.row == 0) {
            
            AboutViewController *avc = [[AboutViewController alloc] init];
            avc.title = @"Help";
            [self.navigationController pushViewController:avc animated:YES];
        }
        return;
    }
    
    if (indexPath.section == 1) {
		if (indexPath.row == 0) {
            TimeViewController *tvc = [[TimeViewController alloc] initWithNibName:nil bundle:nil];
            tvc.title = @"Start";
            [self.navigationController pushViewController:tvc animated:YES];
            
            return;
        }
        if (indexPath.row == 1) {
            TimeViewController *tvc = [[TimeViewController alloc] initWithNibName:nil bundle:nil];
            tvc.title = @"ETA";
            [self.navigationController pushViewController:tvc animated:YES];
            
            return;
        }
    }
    
    if (indexPath.section == 2) {
		if (indexPath.row == 0) {
            if ([model calcRestBreaks]) { // check timings are valid then calc breaks
                AnalysisViewController *avc = [[AnalysisViewController alloc] init];
                avc.title = @"Breaks";
                [self.navigationController pushViewController:avc animated:YES];
            } else {
                // bad, times are out of logical sequence
                UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Please check your entered times for errors."
                                                                         delegate:self
                                                                cancelButtonTitle:nil
                                                           destructiveButtonTitle:@"OK"
                                                                otherButtonTitles:nil];
                
                actionSheet.actionSheetStyle = UIActionSheetStyleDefault;
                [actionSheet showInView:self.view];
            }
        }
        return;
    }
    
    if (indexPath.section == 3) {
        if (indexPath.row == 0) {
            if (model.finalService < 60*60*2) { // max 2 hours
                model.finalService += 60*5; // add 5 mins
            } else {
                model.finalService = 60*60; // back to 1 hour
            }
            [self.tableView reloadData];
            
            [[UIApplication sharedApplication] cancelAllLocalNotifications];
            
            return;
        }
        if (indexPath.row == 1) {
            if (model.handover < 60*15) { // max 15 mins
                model.handover += 60*5; // add 5 mins
            } else {
                model.handover = 0; // back to zero
            }
            [self.tableView reloadData];
            
            return;
        }
		if (indexPath.row == 2) { // number of breaks
            if (model.numberOfBreaks == 2) {
                model.numberOfBreaks = 3;
            } else {
                model.numberOfBreaks = 2;
            }
            [self.tableView reloadData];
            
            [[UIApplication sharedApplication] cancelAllLocalNotifications];
            
            return;
        }
    }
}


@end