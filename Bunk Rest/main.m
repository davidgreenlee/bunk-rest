//
//  main.m
//  Bunk Rest
//
//  Created by David Greenlee on 16/09/2012.
//  Copyright (c) 2012 David Greenlee. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "RestAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([RestAppDelegate class]));
    }
}
