//
//  RestAppDelegate.m
//  Bunk Rest
//
//  Created by David Greenlee on 16/09/2012.
//  Copyright (c) 2012 David Greenlee. All rights reserved.
//

#import "RestAppDelegate.h"
#import "InputViewController.h"
#import "RestModel.h"


@implementation RestAppDelegate

NSString *kAlarmDataKey = @"kAlarmDataKey";

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    
    InputViewController *input = [[InputViewController alloc] init];
    
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:input];
    
    [[self window] setRootViewController:navController];
    
    
    UILocalNotification *notification = [launchOptions objectForKey:UIApplicationLaunchOptionsLocalNotificationKey];
	if (notification)
    {
		NSString *reminderText = [notification.userInfo objectForKey:kAlarmDataKey];
        [self showReminder:reminderText];
	}
	
	application.applicationIconBadgeNumber = 0;
    
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    application.applicationIconBadgeNumber = 0;
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    RestModel *model = [RestModel sharedRestModel];
    [NSTimeZone setDefaultTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    
    NSDate *date = [[NSDate alloc] init];
	NSTimeInterval secs = [date timeIntervalSinceReferenceDate]; // now
	
	model.appBecameActive = secs - fmod(secs, 300.0); // set to integer 5 minutes to match UIPicker
    
    // read userDefaults
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    if ([defaults doubleForKey:@"breaksStart"]) {
        model.breaksStart = [defaults doubleForKey:@"breaksStart"]; // as saved
    } else {
        model.breaksStart = model.appBecameActive; // new
    }
    
    if ([defaults doubleForKey:@"eTA"]) {
        model.eTA = [defaults doubleForKey:@"eTA"]; // as saved
    } else {
        model.eTA = model.appBecameActive + 3600*7; // new
    }
    
    if ([defaults doubleForKey:@"finalService"]) {
        model.finalService = [defaults doubleForKey:@"finalService"]; // as saved
    } else {
        model.finalService = 3600*1; // new
    }
    
    if ([defaults doubleForKey:@"handover"]) {
		model.handover = [defaults doubleForKey:@"handover"]; // as saved, but not if == 0
	} else {
		model.handover = 0; // new
	}
    
    if ([defaults integerForKey:@"numberOfBreaks"]) {
		model.numberOfBreaks = [defaults integerForKey:@"numberOfBreaks"]; // as saved
	} else {
		model.numberOfBreaks = 2; // new
	}
    
    if ([defaults boolForKey:@"alarm0IsOn"]) {
        if (secs < model.break0End) {
            model.alarm0IsOn = YES;
        }
	} else {
		model.alarm0IsOn = NO;
	}
    
    if ([defaults boolForKey:@"alarm1IsOn"]) {
        if (secs < model.break1End) {
            model.alarm1IsOn = YES;
        }
	} else {
		model.alarm1IsOn = NO;
	}
    
    if ([defaults boolForKey:@"alarm2IsOn"]) {
        if (secs < model.break2End) {
            model.alarm2IsOn = YES;
        }
	} else {
		model.alarm2IsOn = NO;
	}
	
    if (fabs(model.appBecameActive - model.breaksStart) > 3600*18) { // if model not set to within 18 hrs
        model.breaksStart = model.appBecameActive; // new
        model.eTA = model.appBecameActive + 3600*7; // default to plus 8 hours
        
        model.break0Start = 0;  // just incase Analysis is displayed
        model.break0End = 0;
        
        model.break1Start = 0;
        model.break1End = 0;
        
        model.break2Start = 0;
        model.break2End = 0;
        
        model.alarm0IsOn = NO;
        model.alarm1IsOn = NO;
        model.alarm2IsOn = NO;
        
        [[UIApplication sharedApplication] cancelAllLocalNotifications]; // just incase
    }
}

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{
	// UIApplicationState state = [application applicationState];
	// if (state == UIApplicationStateInactive) {
    
    // Application was in the background when notification
    // was delivered.
	// }
    
	NSString *alarmText = [notification.userInfo
                           objectForKey:kAlarmDataKey];
	[self showReminder:alarmText]; // no sound
    
    RestModel *model = [RestModel sharedRestModel];
    
    model.alarm0IsOn = NO;
    model.alarm1IsOn = NO;
    model.alarm2IsOn = NO;

}

- (void)showReminder:(NSString *)alarmText
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil
                                                        message:alarmText
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
	[alertView show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
    // use this to save data
    // not sure if a reboot when app is in background calls terminate
    
    RestModel *model = [RestModel sharedRestModel];
	
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	
	[defaults  setDouble:model.breaksStart forKey:@"breaksStart"];
	[defaults  setDouble:model.eTA forKey:@"eTA"];
    [defaults  setDouble:model.finalService forKey:@"finalService"];
    [defaults  setDouble:model.handover forKey:@"handover"];
    [defaults  setInteger:model.numberOfBreaks forKey:@"numberOfBreaks"];
    [defaults  setBool:model.alarm0IsOn forKey:@"alarm0IsOn"];
    [defaults  setBool:model.alarm1IsOn forKey:@"alarm1IsOn"];
    [defaults  setBool:model.alarm2IsOn forKey:@"alarm2IsOn"];
    
    [defaults synchronize];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    RestModel *model = [RestModel sharedRestModel];
	
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	
	[defaults  setDouble:model.breaksStart forKey:@"breaksStart"];
	[defaults  setDouble:model.eTA forKey:@"eTA"];
    [defaults  setDouble:model.finalService forKey:@"finalService"]; 
    [defaults  setDouble:model.handover forKey:@"handover"];
    [defaults  setInteger:model.numberOfBreaks forKey:@"numberOfBreaks"];
    [defaults  setBool:model.alarm0IsOn forKey:@"alarm0IsOn"];
    [defaults  setBool:model.alarm1IsOn forKey:@"alarm1IsOn"];
    [defaults  setBool:model.alarm2IsOn forKey:@"alarm2IsOn"];
    
    [defaults synchronize];
}

@end
