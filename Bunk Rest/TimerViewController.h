//
//  TimerViewController.h
//  Crew Rest
//
//  Created by David Greenlee on 14/09/2012.
//  Copyright (c) 2012 David Greenlee. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TimerViewController : UIViewController

extern NSString *kAlarmDataKey;

@property (nonatomic, weak) IBOutlet UILabel *infoLabel;
@property (nonatomic, weak) IBOutlet UILabel *timerLabel;
@property (nonatomic, weak) NSTimer *timer;
@property (nonatomic, strong) NSString *reminderText;

- (void)updateTimerLabel;
- (NSString *)descriptionHMS:(NSInteger)secs;

@end
