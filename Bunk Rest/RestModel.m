//
//  RestModel.m
//  Crew Rest
//
//  Created by David Greenlee on 13/09/2012.
//  Copyright (c) 2012 David Greenlee. All rights reserved.
//

#import "RestModel.h"

@implementation RestModel

@synthesize breaksStart, eTA, breaksEnd, break0Start, break0End, break1Start, break1End, break2Start, break2End, finalService, alarm0IsOn, alarm1IsOn, alarm2IsOn, handover, numberOfBreaks, appBecameActive;

- (id)init
{
    self = [super init];
    if (self) {
        numberOfBreaks = 2;
        handover = 60*15; // :15
    }
    return self;
}

+ (RestModel *)sharedRestModel
{
    static RestModel *sharedModel = nil;
    if (!sharedModel) {
        sharedModel = [[super allocWithZone:nil] init];
    }
    return sharedModel;
}

- (void)setNumberOfBreaks:(int)numOfBreaks
{
    if (numOfBreaks >= 3) {
        numberOfBreaks = 3;
    } else if (numOfBreaks <= 2) {
        numberOfBreaks = 2;
    }
}

- (BOOL)calcRestBreaks
{
    breaksEnd = eTA - finalService;
    if (breaksEnd < breaksStart + handover*(numberOfBreaks -1) ||  // no time for any rest
        breaksStart - appBecameActive > 3600*4 ||   // starting more than 4 hours in future
        appBecameActive - breaksStart > 3600*21 ||   // starting more than 21 hours ago
        breaksEnd - breaksStart > 3600*14) { 
        
        return NO;
    }
    
    
    if (numberOfBreaks == 2) {
        NSTimeInterval duration = (NSTimeInterval)(breaksEnd - breaksStart - handover)/2.0;
        break0Start = breaksStart;
        break0End = break0Start + duration;
        
        break1Start = break0End + handover;
        break1End = break1Start + duration;
        
        break2End = 0;
    } else {
        NSTimeInterval duration = (NSTimeInterval)(breaksEnd - breaksStart - 2*handover)/3.0;
        break0Start = breaksStart;
        break0End = break0Start + duration;
        
        break1Start = break0End + handover;
        break1End = break1Start + duration;
        
        break2Start = break1End + handover;
        break2End = break2Start + duration;
    }
    return YES;
}

- (NSString *)descriptionForSecs:(NSInteger)secs {
    if (secs < 0) {
        return @"*:**";
    }
	NSInteger remainderSecs = secs%60;
	NSInteger mins = (secs - remainderSecs)/60; // whole mins
	NSInteger remainderMins = mins%60;
	NSInteger hours = (mins - remainderMins)/60; // whole hours
	NSString *text;
	if (remainderMins < 10) {
		text = [NSString stringWithFormat:@"%d:0%d", hours, remainderMins];
	} else {
		text = [NSString stringWithFormat:@"%d:%d", hours, remainderMins];
	}
	return text;
}


@end
