//
//  AboutViewController.m
//  Bunk Rest
//
//  Created by David Greenlee on 19/12/2012.
//  Copyright (c) 2012 David Greenlee. All rights reserved.
//

#import "AboutViewController.h"

@interface AboutViewController ()
@property (nonatomic, weak) IBOutlet UIWebView *aboutWebView;
@end

@implementation AboutViewController
@synthesize aboutWebView;

/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
 */

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    NSString *filename = [[NSBundle mainBundle] pathForResource:@"help" ofType:@"html"];
	NSURL *url = [NSURL fileURLWithPath:filename];
	NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
	
	[self.aboutWebView loadRequest:urlRequest];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
