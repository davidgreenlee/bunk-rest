//
//  DateCategory.m
//  OnLocal
//
//  Created by Jane Greenlee on 26/04/2010.
//  Copyright 2010 DutyHours.co.uk. All rights reserved.
//

#import "DateCategory.h"


@implementation  NSDate (DateCategory)

- (NSString *) detailDescription {
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setDateStyle:NSDateFormatterShortStyle];
    // [dateFormatter setDateStyle:NSDateFormatterNoStyle];
	[dateFormatter setTimeStyle:NSDateFormatterShortStyle];
	
	NSString *formattedDateString = [dateFormatter stringFromDate:self];
	return formattedDateString;
}

- (NSString *) detailShortDescription {
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	// [dateFormatter setDateStyle:NSDateFormatterShortStyle];
    // [dateFormatter setDateStyle:NSDateFormatterNoStyle];
    [dateFormatter setDateFormat:@"HH:mm"];
	// [dateFormatter setTimeStyle:NSDateFormatterLongStyle];
    [dateFormatter setTimeStyle:NSDateFormatterShortStyle];
	
	NSString *formattedDateString = [dateFormatter stringFromDate:self];
    //[formattedDateString substringFromIndex:10];
	return formattedDateString;
}

@end
