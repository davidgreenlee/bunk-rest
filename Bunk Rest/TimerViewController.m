//
//  TimerViewController.m
//  Crew Rest
//
//  Created by David Greenlee on 14/09/2012.
//  Copyright (c) 2012 David Greenlee. All rights reserved.
//

#import "TimerViewController.h"
#import "RestModel.h"
#import "AnalysisViewController.h"

@interface TimerViewController ()

@end

@implementation TimerViewController
@synthesize infoLabel, timerLabel, timer, reminderText;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        [self setReminderText:@"Wakeup reminder"];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewWillAppear:(BOOL)animated
{
    infoLabel.text = @"Countdown";
    if (!self.timer) {
        self.timer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                      target:self
                                                    selector:@selector(updateTimerLabel)
                                                    userInfo:nil
                                                     repeats:YES];
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [timer invalidate];
}

- (void)updateTimerLabel
{
    RestModel *model = [RestModel sharedRestModel];
    
    NSDate *now = [[NSDate alloc] init];
    NSTimeInterval nowSecs = [now timeIntervalSinceReferenceDate];
    NSTimeInterval timeToGo;
    
    if (model.break0End > nowSecs) {
        timeToGo = model.break0End - nowSecs;
    } else if (model.break1End > nowSecs) {
        timeToGo = model.break1End - nowSecs;
    } else if (model.break2End > nowSecs) {
        timeToGo = model.break2End - nowSecs;
    } else {
        [timer invalidate];
        self.timer = nil;
        timerLabel.text = @"0:00:00";
        infoLabel.text = @"Rest breaks over";

        return;
    }
    
    timerLabel.text = [self descriptionHMS:timeToGo];
}

- (NSString *)descriptionHMS:(NSInteger)secs
{
    NSInteger remainderSecs = secs%60;
	NSInteger mins = (secs - remainderSecs)/60; // whole mins
	NSInteger remainderMins = mins%60;
	NSInteger hours = (mins - remainderMins)/60; // whole hours

    NSString *text = [NSString stringWithFormat:@"%d:%02d:%02d", hours, remainderMins, remainderSecs];
	return text;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    self.timer = nil;
    self.infoLabel = nil;
    self.timerLabel = nil;
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
