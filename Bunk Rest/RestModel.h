//
//  RestModel.h
//  Crew Rest
//
//  Created by David Greenlee on 13/09/2012.
//  Copyright (c) 2012 David Greenlee. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RestModel : NSObject

@property (nonatomic) NSTimeInterval breaksStart; // only even 5 mins
@property (nonatomic) NSTimeInterval eTA;         // only even 5 mins
@property (nonatomic) NSTimeInterval breaksEnd;

@property (nonatomic) NSTimeInterval finalService; // only even 5 mins
@property (nonatomic) NSTimeInterval handover;     // only even 5 mins
@property (nonatomic) int numberOfBreaks;          // 2 or 3
@property (nonatomic) BOOL alarm0IsOn;
@property (nonatomic) BOOL alarm1IsOn;
@property (nonatomic) BOOL alarm2IsOn;

@property (nonatomic) NSTimeInterval break0Start;
@property (nonatomic) NSTimeInterval break0End;

@property (nonatomic) NSTimeInterval break1Start;
@property (nonatomic) NSTimeInterval break1End;

@property (nonatomic) NSTimeInterval break2Start;
@property (nonatomic) NSTimeInterval break2End;

@property (nonatomic) NSTimeInterval appBecameActive;  // only even 5 mins

+ (RestModel *)sharedRestModel;
- (NSString *)descriptionForSecs:(NSInteger)secs;
- (BOOL)calcRestBreaks;

@end
