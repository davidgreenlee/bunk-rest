//
//  RestAppDelegate.h
//  Bunk Rest
//
//  Created by David Greenlee on 16/09/2012.
//  Copyright (c) 2012 David Greenlee. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RestAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

extern NSString *kAlarmDataKey;

- (void)showReminder:(NSString *)reminderText;

@end
