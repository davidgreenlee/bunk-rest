//
//  TimeViewController.m
//  Crew Rest
//
//  Created by David Greenlee on 13/09/2012.
//  Copyright (c) 2012 David Greenlee. All rights reserved.
//

#import "TimeViewController.h"
#import "RestModel.h"

@interface TimeViewController ()

@end

@implementation TimeViewController
@synthesize infoLabel, datePicker;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(BOOL)canBecomeFirstResponder {
    return YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIColor *clr = nil;
    clr = [UIColor groupTableViewBackgroundColor];
    
    [[self view] setBackgroundColor:clr];
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
	
	RestModel *model = [RestModel sharedRestModel];
	
	NSDate *date; // = [NSDate date];
	
	
    self.datePicker.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
    
    if ([self.title isEqualToString:@"Start"]) {
        date = [NSDate dateWithTimeIntervalSinceReferenceDate:model.breaksStart];
        [self.datePicker setDate:date animated:NO];
        
        infoLabel.text = @"Rest breaks start (Z)";
    } else if ([self.title isEqualToString:@"ETA"]) {
        date =  [NSDate dateWithTimeIntervalSinceReferenceDate:model.eTA];
        [self.datePicker setDate:date animated:NO];
        
        infoLabel.text = @"Estimated landing time (Z)";
    }
}



- (void)updateModel {
    RestModel *model = [RestModel sharedRestModel];
    // NSLog(@"DateViewDidDisappear");
    
    if ([self.title isEqualToString:@"Start"]) {
        NSDate *start; // = [NSDate date];
        start = [self.datePicker date];
        model.breaksStart = [start timeIntervalSinceReferenceDate];
    } else if ([self.title isEqualToString:@"ETA"]) {
        NSDate *end; // = [NSDate date];
        end = [self.datePicker date];
        model.eTA = [end timeIntervalSinceReferenceDate];
    }
    
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
}

- (void)motionEnded:(UIEventSubtype)motion withEvent:(UIEvent *)event
{
    if (motion == UIEventSubtypeMotionShake)
    {
        // your code
        // NSLog(@"reset timings");
        
        RestModel *model = [RestModel sharedRestModel];
        
        [NSTimeZone setDefaultTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
        
        NSDate *date = [[NSDate alloc] init];
        NSTimeInterval secs = [date timeIntervalSinceReferenceDate];
        
        secs = secs - fmod(secs, 300.0); // now set to integer 5 minutes to match UIPicker
        
        if ([self.title isEqualToString:@"Start"]) {
            model.appBecameActive = secs; // now
            model.breaksStart = secs;
            NSDate *startDate = [NSDate dateWithTimeIntervalSinceReferenceDate:model.breaksStart];
            [self.datePicker setDate:startDate animated:YES];
        } else {
            model.eTA = secs + 7*3600; // now + 7 hours
            NSDate *eTADate = [NSDate dateWithTimeIntervalSinceReferenceDate:model.eTA];
            [self.datePicker setDate:eTADate animated:YES];
        }
        
        [[UIApplication sharedApplication] cancelAllLocalNotifications];
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [self updateModel];
    [self resignFirstResponder];
    
    [super viewWillDisappear:animated];
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    self.datePicker = nil;
    self.infoLabel = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
