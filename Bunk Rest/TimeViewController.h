//
//  TimeViewController.h
//  Crew Rest
//
//  Created by David Greenlee on 13/09/2012.
//  Copyright (c) 2012 David Greenlee. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TimeViewController : UIViewController

@property (nonatomic, weak) IBOutlet UIDatePicker *datePicker;
@property (nonatomic, weak) IBOutlet UILabel *infoLabel;


@end
