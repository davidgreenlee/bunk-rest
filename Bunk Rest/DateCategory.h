//
//  DateCategory.h
//  OnLocal
//
//  Created by Jane Greenlee on 26/04/2010.
//  Copyright 2010 DutyHours.co.uk. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NSDate (DateCategory)

- (NSString *) detailDescription;
- (NSString *) detailShortDescription;

@end
