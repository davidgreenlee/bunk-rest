//
//  AnalysisViewController.m
//  Crew Rest
//
//  Created by David Greenlee on 13/09/2012.
//  Copyright (c) 2012 David Greenlee. All rights reserved.
//

#import "AnalysisViewController.h"
#import "RestModel.h"
#import "DateCategory.h"
#import "TimerViewController.h"

@interface AnalysisViewController ()

@end

@implementation AnalysisViewController
@synthesize interfaceDetailData, reminderText;


- (id)init
{
    self = [super initWithStyle:UITableViewStyleGrouped];
    if (self) {
        // Custom initialization
        [self setTitle:@"Breaks"];
        [self setReminderText:@"Wakeup reminder"];
        
        UIBarButtonItem *timerView = [[UIBarButtonItem alloc] initWithTitle:@"Countdown" style:UIBarButtonItemStyleBordered target:self action:@selector(showTimerView)];
        [self.navigationItem setRightBarButtonItem:timerView];
    }
    return self;
}

- (id)initWithStyle:(UITableViewStyle)style
{
    return [self init];
}

- (void)showTimerView
{
    TimerViewController *tvc = [[TimerViewController alloc] initWithNibName:nil bundle:nil];
    [self.navigationController pushViewController:tvc animated:YES];
}

- (void)scheduleNotification // same code in TimerViewController.m. It can 
{    // get called perhaps more than once depending on user selections.
    
    RestModel *model = [RestModel sharedRestModel];
    
	[[UIApplication sharedApplication] cancelAllLocalNotifications];
    // NSLog(@"cancel all");
    
    [NSTimeZone setDefaultTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    NSDate *now = [[NSDate alloc] init];
    NSTimeInterval nowSecs = [now timeIntervalSinceReferenceDate];
    
    NSDictionary *userDict = [NSDictionary dictionaryWithObject:self.reminderText
                                                         forKey:kAlarmDataKey];
    
    // alarm0
    UILocalNotification *notif0 = [[UILocalNotification alloc] init];
    NSDate *alarm0 = [[NSDate alloc] initWithTimeIntervalSinceReferenceDate:model.break0End];
    
    if (model.break0End > nowSecs) {
        // NSLog(@"schedule alarm 0");
        notif0.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
        notif0.fireDate = alarm0;
        notif0.alertBody = self.reminderText;
        notif0.alertAction = @"OK";
        notif0.soundName = @"doorBellSound.caf"; // UILocalNotificationDefaultSoundName;
        // afconvert -v -f 'm4af' doorBellSound.m4a doorBellSound.caf
        notif0.userInfo = userDict;
        [[UIApplication sharedApplication] scheduleLocalNotification:notif0];
    }

    // alarm1
    UILocalNotification *notif1 = [[UILocalNotification alloc] init];
    NSDate *alarm1 = [[NSDate alloc] initWithTimeIntervalSinceReferenceDate:model.break1End];
    
    if (model.break1End > nowSecs) {
        // NSLog(@"schedule alarm 1");
        notif1.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
        notif1.fireDate = alarm1;
        notif1.alertBody = self.reminderText;
        notif1.alertAction = @"OK";
        notif1.soundName = @"doorBellSound.caf"; //UILocalNotificationDefaultSoundName;
        notif1.userInfo = userDict;
        [[UIApplication sharedApplication] scheduleLocalNotification:notif1];
    }
    
    if (model.numberOfBreaks == 3) {
        // alarm2
        UILocalNotification *notif2 = [[UILocalNotification alloc] init];
        NSDate *alarm2 = [[NSDate alloc] initWithTimeIntervalSinceReferenceDate:model.break2End];
        
        if (model.break2End > nowSecs) {
            // NSLog(@"schedule alarm 2");
            notif2.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
            notif2.fireDate = alarm2;
            notif2.alertBody = self.reminderText;
            notif2.alertAction = @"OK";
            notif2.soundName = @"doorBellSound.caf"; //UILocalNotificationDefaultSoundName;
            notif2.userInfo = userDict;
            [[UIApplication sharedApplication] scheduleLocalNotification:notif2];
        }
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewWillAppear:(BOOL)animated {
    RestModel *model = [RestModel sharedRestModel];
   
    NSDate *date = [[NSDate alloc] init];
	NSTimeInterval secs = [date timeIntervalSinceReferenceDate]; // now
    
    if (model.alarm0IsOn &&
        secs > model.break0End) {
        model.alarm0IsOn = NO;
	}
    
    if (model.alarm1IsOn &&
        secs > model.break1End) {
        model.alarm1IsOn = NO;
	}
    
    if (model.alarm2IsOn &&
        secs > model.break2End) {
        model.alarm2IsOn = NO;
	}
    
	[self setupInterface];
	[self.tableView reloadData];
    
    /*
    if (model.alarm0IsOn) {
        [self scheduleNotification]; // set alarms if appropriate (still time to run)
    } else {
        [[UIApplication sharedApplication] cancelAllLocalNotifications];
    }
     */
}

- (void)setupInterface
{
    RestModel *model = [RestModel sharedRestModel];
    
    NSArray *detailSection0;
    NSArray *detailSection1;
    NSArray *detailSection2;
    
    NSDate *start0 = [NSDate dateWithTimeIntervalSinceReferenceDate:model.break0Start];
    NSDate *end0 = [NSDate dateWithTimeIntervalSinceReferenceDate:model.break0End];
    
    detailSection0 = [[NSArray alloc]initWithObjects:[start0 detailShortDescription], [end0 detailShortDescription], nil];
    
    NSDate *start1 = [NSDate dateWithTimeIntervalSinceReferenceDate:model.break1Start];
    NSDate *end1 = [NSDate dateWithTimeIntervalSinceReferenceDate:model.break1End];
    detailSection1 = [[NSArray alloc]initWithObjects:[start1 detailShortDescription], [end1 detailShortDescription], nil];
    
    if (model.numberOfBreaks == 2) {
        interfaceDetailData = [[NSArray alloc] initWithObjects:detailSection0, detailSection1, nil];
    } else {
        NSDate *start2 = [NSDate dateWithTimeIntervalSinceReferenceDate:model.break2Start];
        NSDate *end2 = [NSDate dateWithTimeIntervalSinceReferenceDate:model.break2End];
        detailSection2 = [[NSArray alloc]initWithObjects:[start2 detailShortDescription], [end2 detailShortDescription], nil];
        interfaceDetailData = [[NSArray alloc] initWithObjects:detailSection0, detailSection1, detailSection2, nil];
    }
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark Table view methods

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    RestModel *model = [RestModel sharedRestModel];
    
    NSTimeInterval duration = model.break0End - model.break0Start;
    NSString *durationString = [model descriptionForSecs:duration];
	switch (section) {
		case 0: return [NSString stringWithFormat:@"First rest = %@", durationString];
			break;
		case 1: return [NSString stringWithFormat:@"Second rest = %@", durationString];
			break;
        case 2: return [NSString stringWithFormat:@"Third rest = %@", durationString];
            
		default: return nil;
	}
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    RestModel *model = [RestModel sharedRestModel];
    return model.numberOfBreaks;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:nil];
    
    cell.backgroundColor = [UIColor whiteColor];
    
    RestModel *model = [RestModel sharedRestModel];
    [NSTimeZone setDefaultTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    NSDate *now = [[NSDate alloc] init];
    NSTimeInterval nowSecs = [now timeIntervalSinceReferenceDate];
    
    if (indexPath.section == 0) {
        if (indexPath.row == 0) { // Breaks start
            [[cell textLabel] setText:@"Start"];
            
            NSDate *start = [NSDate dateWithTimeIntervalSinceReferenceDate:model.break0Start];
            cell.detailTextLabel.text = [start detailShortDescription];
        } else if (indexPath.row == 1) { // Breaks end
            [NSTimeZone setDefaultTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
            NSDate *end = [NSDate dateWithTimeIntervalSinceReferenceDate:model.break0End];
            cell.detailTextLabel.text = [end detailShortDescription];
            
            // if alarm 0 is set
            if (model.alarm0IsOn) {
                if (model.break0End > nowSecs) { // alarm is still to be reached
                    cell.backgroundColor = [UIColor yellowColor];
                    [[cell textLabel] setText:@"End - alarm set"];
                } else {
                    model.alarm0IsOn = NO;
                    cell.backgroundColor = [UIColor whiteColor];
                    [[cell textLabel] setText:@"End"];
                }
            } else {
                cell.backgroundColor = [UIColor whiteColor];
                if (model.break0End > nowSecs) {
                    [[cell textLabel] setText:@"End - tap to set alarm"];
                } else {
                    [[cell textLabel] setText:@"End"];
                }
            }
        }
    }
    if (indexPath.section == 1) {
        if (indexPath.row == 0) { // Breaks start
            [[cell textLabel] setText:@"Start"];
            
            NSDate *start = [NSDate dateWithTimeIntervalSinceReferenceDate:model.break1Start];
            cell.detailTextLabel.text = [start detailShortDescription];
        } else if (indexPath.row == 1) { // Breaks end
            [NSTimeZone setDefaultTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
            NSDate *end = [NSDate dateWithTimeIntervalSinceReferenceDate:model.break1End];
            cell.detailTextLabel.text = [end detailShortDescription];
            
            // if alarm 1 is set
            if (model.alarm1IsOn) {
                if (model.break1End > nowSecs) { // alarm is still to be reached
                    cell.backgroundColor = [UIColor yellowColor];
                    [[cell textLabel] setText:@"End - alarm set"];
                } else {
                    model.alarm1IsOn = NO;
                    cell.backgroundColor = [UIColor whiteColor];
                    [[cell textLabel] setText:@"End"];
                }
            } else {
                cell.backgroundColor = [UIColor whiteColor];
                if (model.break1End > nowSecs) {
                    [[cell textLabel] setText:@"End - tap to set alarm"];
                } else {
                    [[cell textLabel] setText:@"End"];
                }
            }
        }
    }
    if (indexPath.section == 2) {
        // if 3 breaks
        if (indexPath.row == 0) { // Breaks start
            [[cell textLabel] setText:@"Start"];
            [NSTimeZone setDefaultTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
            NSDate *start = [NSDate dateWithTimeIntervalSinceReferenceDate:model.break2Start];
            cell.detailTextLabel.text = [start detailShortDescription];
        } else if (indexPath.row == 1) { // Breaks end
            
            NSDate *end = [NSDate dateWithTimeIntervalSinceReferenceDate:model.break2End];
            cell.detailTextLabel.text = [end detailShortDescription];
            
            // if alarm 02 is set
            if (model.alarm2IsOn) {
                if (model.break2End > nowSecs) { // alarm is still to be reached
                    cell.backgroundColor = [UIColor yellowColor];
                    [[cell textLabel] setText:@"End - alarm set"];
                } else {
                    model.alarm2IsOn = NO;
                    cell.backgroundColor = [UIColor whiteColor];
                    [[cell textLabel] setText:@"End"];
                }
            } else {
                cell.backgroundColor = [UIColor whiteColor];
                if (model.break2End > nowSecs) {
                    [[cell textLabel] setText:@"End - tap to set alarm"];
                } else {
                    [[cell textLabel] setText:@"End"];
                }
            }
        }
    }
    return cell;
}

// tap cell to set an alarm
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    RestModel *model = [RestModel sharedRestModel];
    
    if (indexPath.section == 0) {
        if (indexPath.row == 1) { // break0 end
            if (model.alarm0IsOn) {
                model.alarm0IsOn = NO;
                [[UIApplication sharedApplication] cancelAllLocalNotifications];
            } else {
                // only allow 1 alarm
                [[UIApplication sharedApplication] cancelAllLocalNotifications];
                model.alarm1IsOn = NO;
                model.alarm2IsOn = NO;
                // try to set alarm0
                [NSTimeZone setDefaultTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
                NSDate *now = [[NSDate alloc] init];
                NSTimeInterval nowSecs = [now timeIntervalSinceReferenceDate];
                if (model.break0End > nowSecs) { // OK to set
                    // [[UIApplication sharedApplication] cancelAllLocalNotifications];
                    model.alarm0IsOn = YES;
                    NSDictionary *userDict = [NSDictionary dictionaryWithObject:self.reminderText
                                                                         forKey:kAlarmDataKey];
                    
                    // alarm0
                    UILocalNotification *notif = [[UILocalNotification alloc] init];
                    NSDate *alarm = [[NSDate alloc] initWithTimeIntervalSinceReferenceDate:model.break0End];
                    // NSLog(@"schedule alarm 0");
                    notif.timeZone = nil; // [NSTimeZone timeZoneForSecondsFromGMT:0];
                    notif.fireDate = alarm;
                    notif.alertBody = self.reminderText;
                    notif.alertAction = @"OK";
                    notif.soundName = @"doorBellSound.caf"; // UILocalNotificationDefaultSoundName;
                    // afconvert -v -f 'm4af' doorBellSound.m4a doorBellSound.caf
                    notif.userInfo = userDict;
                    [[UIApplication sharedApplication] scheduleLocalNotification:notif];
                }
            }
        }
    }
    if (indexPath.section == 1) {
        if (indexPath.row == 1) { // break0 end
            if (model.alarm1IsOn) {
                model.alarm1IsOn = NO;
                [[UIApplication sharedApplication] cancelAllLocalNotifications];
            } else {
                // only allow 1 alarm
                [[UIApplication sharedApplication] cancelAllLocalNotifications];
                model.alarm0IsOn = NO;
                model.alarm2IsOn = NO;
                // try to set alarm1
                [NSTimeZone setDefaultTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
                NSDate *now = [[NSDate alloc] init];
                NSTimeInterval nowSecs = [now timeIntervalSinceReferenceDate];
                if (model.break1End > nowSecs) { // OK to set
                    [[UIApplication sharedApplication] cancelAllLocalNotifications];
                    model.alarm1IsOn = YES;
                    NSDictionary *userDict = [NSDictionary dictionaryWithObject:self.reminderText
                                                                         forKey:kAlarmDataKey];
                    
                    // alarm
                    UILocalNotification *notif = [[UILocalNotification alloc] init];
                    NSDate *alarm = [[NSDate alloc] initWithTimeIntervalSinceReferenceDate:model.break1End];
                    // NSLog(@"schedule alarm 0");
                    notif.timeZone = nil; // [NSTimeZone timeZoneForSecondsFromGMT:0];
                    notif.fireDate = alarm;
                    notif.alertBody = self.reminderText;
                    notif.alertAction = @"OK";
                    notif.soundName = @"doorBellSound.caf"; // UILocalNotificationDefaultSoundName;
                    // afconvert -v -f 'm4af' doorBellSound.m4a doorBellSound.caf
                    notif.userInfo = userDict;
                    [[UIApplication sharedApplication] scheduleLocalNotification:notif];
                }
            }
        }
    }
    if (indexPath.section == 2) {
        if (indexPath.row == 1) { // break2 end
            if (model.alarm2IsOn) {
                model.alarm2IsOn = NO;
                [[UIApplication sharedApplication] cancelAllLocalNotifications];
            } else {
                // only allow 1 alarm
                [[UIApplication sharedApplication] cancelAllLocalNotifications];
                model.alarm0IsOn = NO;
                model.alarm1IsOn = NO;
                
                // try to set alarm0
                [NSTimeZone setDefaultTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
                NSDate *now = [[NSDate alloc] init];
                NSTimeInterval nowSecs = [now timeIntervalSinceReferenceDate];
                if (model.break2End > nowSecs) { // OK to set
                    [[UIApplication sharedApplication] cancelAllLocalNotifications];
                    model.alarm2IsOn = YES;
                    NSDictionary *userDict = [NSDictionary dictionaryWithObject:self.reminderText
                                                                         forKey:kAlarmDataKey];
                    
                    // alarm
                    UILocalNotification *notif = [[UILocalNotification alloc] init];
                    NSDate *alarm = [[NSDate alloc] initWithTimeIntervalSinceReferenceDate:model.break2End];
                    // NSLog(@"schedule alarm 0");
                    notif.timeZone = nil; // [NSTimeZone timeZoneForSecondsFromGMT:0];
                    notif.fireDate = alarm;
                    notif.alertBody = self.reminderText;
                    notif.alertAction = @"OK";
                    notif.soundName = @"doorBellSound.caf"; // UILocalNotificationDefaultSoundName;
                    // afconvert -v -f 'm4af' doorBellSound.m4a doorBellSound.caf
                    notif.userInfo = userDict;
                    [[UIApplication sharedApplication] scheduleLocalNotification:notif];
                }
            }
        }
    }

    [self.tableView reloadData];
}

@end
